
API Weather GraphQL 

============

  
  

This project was created to show how we use GraphQL with  Yahoo Weather API.

 
## How to run

To run backend you need clone this repo and:

  

* If you have docker installed, just type this in project root:

  

> docker-compose build
> docker-compose up -d

  

This command will run containers.

  

API endpoint: `http://localhost:80/graphql`

  
  

* If you have web stack installed, run:

  

> php bin/console server:run

  

API endpoint: `http://localhost:8000/graphql`

  

## Sending requests

Now you can easily send request to endpoint, for example:

>http://127.0.0.1:8000/graphql

  

Response:

```graphql

{

"errors": [

{

"message": "Must provide an operation."

}

]

}

  

```

Now let's specify query in request content:
> http://127.0.0.1:8000/graphql?query={weather(city:rabat){humidity,pressure,location}}

Response:
``` graphql
{
    "data": {
        "weather": {
            "humidity": 68,
            "pressure": 29.8,
            "location": "Rabat"
        }
    }
}
```

  
## Sample query

```

{
    weather(city: rabat) {
        temperature,
        humidity,
        pressure,
        location,
        wind_speed,
        wind_direction
    }
}

```

Response:

```

{
    "data": {
        "weather": {
            "temperature": 72,
            "humidity": 68,
            "pressure": 29.8,
            "location": "Rabat",
            "wind_speed": 8.08,
            "wind_direction": 305
        }
    }
}

...

```
 