<?php
namespace App\GraphQL\Types;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Youshido\GraphQL\Type\AbstractType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Field\AbstractField as BaseAbstractField;
use App\Service\WeatherService;

abstract class AbstractField extends BaseAbstractField implements ContainerAwareInterface
{
    use ContainerAwareTrait;


    /**
     * @return WeatherService
     */
    protected function getWeatherService()
    {
        $service = $this->container->get(WeatherService::class);
        return $service;
    }
}
