<?php
namespace App\GraphQL\Types\Weather\Fields;

use App\Entity\Weather;
use App\GraphQL\Types\AbstractField;
use App\GraphQL\Types\Weather\WeatherType;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;

class WeatherField extends AbstractField
{
    public function __construct(array $config = [])
    {
        $config["args"]= [
            'city' =>
                ['type' => new NonNullType(new IdType()),
                    'description' => 'Unique Weather City'
                ],
        ];
        parent::__construct($config);
    }

    public function getType()
    {
        return  new WeatherType();
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        $weather = $this->getWeatherService()->getByCityName($args['city']);
        return $weather;
    }
}
